using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Garante que há um inimigo selecionado, para não gerar erros do nada
[RequireComponent(typeof(Enemy))]
public class EnemyHealth : MonoBehaviour
{
    [SerializeField] int maxHitPoints = 5;
    [Tooltip("Adds amount to max hitpoints when enemy dies")]
    [SerializeField] int difficultyRamp = 1;
    int currentHitPoints = 0;

    Enemy enemy;

    // Start is called before the first frame update
    void OnEnable()
    {
        currentHitPoints = maxHitPoints;
    }
    void Start() 
    {
        enemy = GetComponent<Enemy>();
    }
    void OnParticleCollision(GameObject other) 
    {
        ProcessHit();
    }

    // Check the "Send Collision Message" checkbox on the particle system inside the tower prefab, 
    // otherwise the collision will not update
    void ProcessHit()
    {
        currentHitPoints--;
        if(currentHitPoints <= 0)
        {
            gameObject.SetActive(false);
            maxHitPoints += difficultyRamp;
            enemy.RewardGold();
        }
    }
}
